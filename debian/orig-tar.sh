#!/bin/sh

# called by uscan with '--upstream-version' <version> <file>
tar xzf $3
rm -rf Vol-*/CoinUtils
rm -f $3
tar czf $3 Vol-*
rm -rf Vol-*

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $3 $origDir
  echo "moved $3 to $origDir"
fi

exit 0
